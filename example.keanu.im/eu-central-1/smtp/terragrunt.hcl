# Include all settings from the root terragrunt.hcl file
locals {
  name    = "smtp"
  project = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  commonx = yamldecode(sops_decrypt_file("${local.project.locals.data_dir}/common.sops.yml"))

  matrix_dns_names   = split(",", local.commonx.matrix_dns_names)
  matrix_common_name = local.commonx.matrix_common_name
  riot_dns_names     = split(",", local.commonx.riot_dns_names)
  riot_common_name   = local.commonx.riot_common_name

}

terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//smtp?ref=master"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name = local.name

  domain_name              = local.commonx.email_root_domain
  mail_from_domain         = local.commonx.email_from_domain
  cloudflare_zone_id       = local.commonx.cloudflare_zone_id
  cloudflare_dns_api_token = local.commonx.cloudflare_dns_api_token
}
