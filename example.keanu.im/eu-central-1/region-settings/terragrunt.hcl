# Include all settings from the root terragrunt.hcl file
locals {
  region = read_terragrunt_config(find_in_parent_folders("region.hcl"))
}
terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//region-settings?ref=master"
}


include {
  path = find_in_parent_folders()
}

inputs = {
  name = local.region.locals.aws_region
}
