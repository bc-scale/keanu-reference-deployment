# Include all settings from the root terragrunt.hcl file
locals {
  project = read_terragrunt_config(find_in_parent_folders("project.hcl"))
  name    = "matrix"
  commonx = yamldecode(sops_decrypt_file("${local.project.locals.data_dir}/common.sops.yml"))
}
terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//matrix-ec2?ref=master"
}

dependencies {
  paths = ["../vpc", "../account-settings", "../region-settings", "../matrix-ssm", "../s3_media", "../letsencrypt", "../route53-internal"]
}

dependency "vpc" {
  config_path = "../vpc"
}

dependency "account_settings" {
  config_path = "../account-settings"
}

dependency "region_settings" {
  config_path = "../region-settings"
}

dependency "matrix_ssm" {
  config_path = "../matrix-ssm"
}

dependency "s3_media" {
  config_path = "../s3-media"
}

dependency "tls" {
  config_path = "../letsencrypt"
}

dependency "route53" {
  config_path = "../route53-internal"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name = local.name

  ma1sd_instance_type           = local.commonx.ma1sd.instance_type
  ma1sd_ssm_prefix              = dependency.matrix_ssm.outputs.ssm_prefix_ma1sd
  ssm_prefix_ma1sd_policy_arn   = dependency.matrix_ssm.outputs.ssm_prefix_ma1sd_policy_arn
  sygnal_instance_type          = local.commonx.sygnal.instance_type
  sygnal_ssm_prefix             = dependency.matrix_ssm.outputs.ssm_prefix_sygnal
  ssm_prefix_sygnal_policy_arn  = dependency.matrix_ssm.outputs.ssm_prefix_sygnal_policy_arn
  synapse_instance_type         = local.commonx.synapse.instance_type
  synapse_ssm_prefix            = dependency.matrix_ssm.outputs.ssm_prefix_synapse
  synapse_disk_allocation_gb    = local.commonx.synapse.synapse_disk_allocation_gb
  ssm_prefix_synapse_policy_arn = dependency.matrix_ssm.outputs.ssm_prefix_synapse_policy_arn
  kms_key_arn                   = dependency.matrix_ssm.outputs.kms_key_arn
  session_manager_bucket        = dependency.region_settings.outputs.ssm_playbook.ssm_logs_bucket
  secretsmanager_ssh_key_id     = dependency.region_settings.outputs.secretsmanager_ssh_key_id
  security_group_id_general     = dependency.vpc.outputs.ssh_security_group_id
  vpc_id                        = dependency.vpc.outputs.vpc.vpc_id
  vpc_cidr_block                = dependency.vpc.outputs.vpc.vpc_cidr_block
  availability_zone_1           = dependency.vpc.outputs.availability_zones[0]
  subnet_id                     = dependency.vpc.outputs.private_subnet_ids[0]
  alb_public_subnets            = dependency.vpc.outputs.public_subnet_ids
  federation_enabled            = true
  media_bucket_arn              = dependency.s3_media.outputs.bucket_arn
  cloudflare_zone_id            = local.commonx.cloudflare_zone_id
  cloudflare_dns_api_token      = local.commonx.cloudflare_dns_api_token
  domain_name                   = local.commonx.matrix_common_name
  certificate_pem               = dependency.tls.outputs.matrix_certificate_pem
  issuer_pem                    = dependency.tls.outputs.matrix_issuer_pem
  private_key_pem               = dependency.tls.outputs.matrix_private_key_pem
  private_zone_id               = dependency.route53.outputs.zone_id
  alb_log_bucket_id             = dependency.account_settings.outputs.lb_log_bucket_id
  alb_log_bucket_prefix         = "alb/matrix-stack"
}
