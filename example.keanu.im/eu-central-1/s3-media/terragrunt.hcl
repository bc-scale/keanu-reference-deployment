# Include all settings from the root terragrunt.hcl file
locals {
  name = "s3-media"
}

terraform {
  source = "git::https://gitlab.com/guardianproject-ops/terraform-modules-keanu//s3-media?ref=master"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  name = local.name

  expiration_days = 90
  force_destroy   = false
}
